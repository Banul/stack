package info.androidhive.stacksearch;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by User on 2017-05-20.
 */

// włącza ekran stackoverflow
public class WebViewActivity extends Activity
{

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview_layout);
        String s = getIntent().getStringExtra("URL");

       WebView webview = (WebView) findViewById(R.id.webview1);
                   webview.setWebViewClient(new WebViewClient());
                    webview.loadUrl(s);

    }


}
