package info.androidhive.stacksearch;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by User on 2017-05-19.
 */

//domyślna klasa ViewPager pozwala przesunąć kciukiem na kolejny ekran, a ta nie pozwala tego zrobić
public class MyViewPager extends ViewPager {
    private boolean pagingEnabled = false; //tutaj zmieniamy, aby nie dało się przesuwać palcem do następnego obrazu


    public MyViewPager(Context context) {
        super(context);
    }

    public MyViewPager (Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (!pagingEnabled) {
            return false; // do not intercept
        }
        return super.onInterceptTouchEvent(event);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (!pagingEnabled) {
            return false; // do not consume
        }
        return super.onTouchEvent(event);
    }
}