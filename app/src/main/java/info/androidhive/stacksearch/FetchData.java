package info.androidhive.stacksearch;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 2017-05-16.
 */

//pobiera dane z API
 class FetchData extends AsyncTask<Void,Void,ArrayList<ItemToList>> {

    private ProgressDialog mDialog; // pokaz userowi, że czekamy
    private Context mContext; // kontekst potrzebny do uruchomienia dialogu
    private String mIntitle;
    private HttpURLConnection mUrlConnection;
    private String [][] info = new String[4][20];
    private CallbackFromAsync <List <ItemToList>> mCallback;
    private ArrayList <ItemToList> items=new ArrayList<>(); // lista na obiekty które będziemy wyświetlać

     FetchData(Context context, String intitle,CallbackFromAsync callback) {
        this.mContext = context;
        this.mIntitle = intitle;
        this.mCallback = callback;
    }

    protected void onPreExecute() {
        mDialog = ProgressDialog.show(mContext, "", "Prosze czekac na wczytanie sie aplikacji", true);
        mDialog.show();


    }

    @Override
    protected ArrayList<ItemToList> doInBackground(Void... params) {
//
        try {

            //wyszukujemy tutaj najnowsze 20 wpisów
            URL url = new URL( "https://api.stackexchange.com/2.2/search?pagesize=20&order=desc&sort=activity&intitle="+mIntitle+"&site=stackoverflow");

            mUrlConnection = (HttpURLConnection) url.openConnection();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(mUrlConnection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line).append("\n");
            }
            bufferedReader.close();
            String JsonString = stringBuilder.toString(); //obiekt który parsujemy metodą ParseJson
            ParseJson(JsonString);
            return items;
        }
        catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            mUrlConnection.disconnect();
        }
    }


    protected void onPostExecute(ArrayList<ItemToList> response) {
        mDialog.dismiss();
        if (response == null || response.size()==0) {
            Toast.makeText(mContext,"No Results",Toast.LENGTH_LONG).show();
        }
        if(mCallback!=null)
        {
            mCallback.change(response);
        }
    }


    private void ParseJson(String JsonString)
    {
        try {
            JSONObject jsonObject = new JSONObject(JsonString);
            JSONArray jsonArray = jsonObject.getJSONArray("items");
            int iterator = 0;

            String authorName;
            String numberOfAnswers;
            String tittle;
            String link;

            while(iterator < jsonArray.length())
            {
                JSONObject JObject = jsonArray.getJSONObject(iterator);
                numberOfAnswers=JObject.getString("answer_count");
                tittle = JObject.getString("title");
                link = JObject.getString("link");
                JSONObject owner = JObject.getJSONObject("owner");
                authorName = owner.getString("display_name");
                info[0][iterator]=numberOfAnswers;
                info[1][iterator]=tittle;
                info[2][iterator]=link;
                info[3][iterator]=authorName;
                items.add(new ItemToList(info[2][iterator], info[3][iterator], info[0][iterator], info[1][iterator]));
                iterator++;

            }

        }
        catch (JSONException e) {
            e.printStackTrace();
            e.getMessage();
            e.getCause();

        }

    }



}



