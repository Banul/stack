package info.androidhive.stacksearch;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity
{

    private EditText editText;
    private String whatToSearch;
    private Context context = this;
    private MyViewPager mViewPager;
    private ArrayList<ItemToList> listOfItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText =(EditText) findViewById(R.id.editText);

        Button button = (Button) findViewById(R.id.SearchButton);
        mViewPager = (MyViewPager) findViewById(R.id.container); // tam gdzie wyswietlamy fragmenty
        SetupPager(mViewPager);
        mViewPager.setCurrentItem(0); // na początku wyświetlamy fragment pierwszy
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                whatToSearch = String.valueOf(editText.getText());
                whatToSearch = whatToSearch.replace(" ","%20"); // tak są reprezentowane spacje - jako %20
                FetchData fetch = new FetchData(context, whatToSearch, new CallbackFromAsync() {

                    //callback z postExecute
                    @Override
                    public void change(ArrayList response) {

                        listOfItems = response; // response to jest to co zwróciliśmy z doInBackground
                        SetupPager(mViewPager);

                        if (response != null && response.size() != 0) {
                            populateListView(); // stwórz listę wyników
                            mViewPager.setCurrentItem(1); // pokaż tę listę
                        } else {
                            mViewPager.setCurrentItem(0); // brak odpowiedzi to zostajemy przy 1 widoku

                        }
                    }
                });

                fetch.execute();

            }
        });

    }

// tworzymy adapter i dodajemy go do pagera, czyli jakby dodajemy do kontenera(myViewPager) adapter z 2 fragmentami
    public void SetupPager(ViewPager MyviewPager)
    {
        SectionsStatePagerAdapter adapter = new SectionsStatePagerAdapter(getSupportFragmentManager());
        adapter.addFragment(  new HomeFragment() ); //fragment o indeksie 0, będzie się domyślnie pokazywać
        adapter.addFragment(  new Listfragment() ); //fragment który będzie się pokazywać potem
        MyviewPager.setAdapter(adapter);
    }

//zapełniamy liste
    private void populateListView()
    {
        ArrayAdapter<ItemToList> adapter = new MyListAdapter();
        ListView list = (ListView) findViewById(R.id.listView);
        list.setAdapter(adapter);

    }

// Tworzenie widoku wiersza na liście
    private class MyListAdapter extends ArrayAdapter<ItemToList> {

         MyListAdapter() {
            super(MainActivity.this, R.layout.row_layout, listOfItems);
        }

        @NonNull
        @Override
        // generuje wiersz listy
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
           //Upewniamy sie że mamy widok
            View itemView = convertView;
            if (itemView == null)
            {
                // zrenderuj widok
                itemView=getLayoutInflater().inflate(R.layout.row_layout,parent,false);

            }
            // zapełniaj elementy tego widoku danymi z listy
            TextView authorName = (TextView) itemView.findViewById(R.id.AuthorName);
            String name = listOfItems.get(position).getAuthorName();
            authorName.setText(name);

            TextView answers = (TextView) itemView.findViewById(R.id.answers);
            String answersNum = listOfItems.get(position).getAnswers();
            answers.setText(answersNum);

            TextView title = (TextView) itemView.findViewById(R.id.Title);
            String titleString = listOfItems.get(position).getTitle();
            title.setText(titleString);

            final String link = listOfItems.get(position).getLink(); // link do postu na stackOverflow

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                // po naciśnieciu wiersza pokaz ekran ze stack overflow
                public void onClick(View v) {
                    Intent intent = new Intent(getBaseContext(), WebViewActivity.class);
                    intent.putExtra("URL", link);
                    startActivity(intent);
                }
            });

            return itemView;
        }
    }


}
