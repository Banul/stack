package info.androidhive.stacksearch;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.Fragment;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 2017-05-18.
 */

// klasa która będzie zawierać nasze 2 fragmenty
 class SectionsStatePagerAdapter extends FragmentStatePagerAdapter {

    private final List<Fragment> mFragmentList = new ArrayList<>();

     SectionsStatePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

     void addFragment(Fragment fragment)
    {
        mFragmentList.add(fragment);
    }
}
