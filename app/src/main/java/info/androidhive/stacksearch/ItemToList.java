package info.androidhive.stacksearch;


/**
 * Created by User on 2017-05-19.
 */

//reprezentacja wiersza w naszej liście z wynikami zapytania
 class ItemToList {
    private String link;
    private String authorName;
    private String answers;
    private String title;

    String getLink() {
        return link;
    }

    String getAuthorName() {
        return authorName;
    }

    String getAnswers() {
        return answers;
    }

    String getTitle() {
        return title;
    }

    ItemToList(String link, String authorName, String answers, String title) {
        this.link = link;
        this.authorName = authorName;
        this.answers = answers;
        this.title = title;
    }
}



